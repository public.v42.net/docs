# Docker container access to local services

Docker allows for building modular applications where each module runs in its own container: 
for instance a database container and a webserver container. With both containers running on
separate hosts everything functions normally, but with both containers active on the same 
docker host, a network issue occured: the containers were unable to communicate to each other.

The docker host being used is a very basic Oracle Linux 7 installation which by default has
`firewalld` (or `iptables`) configured to only allow external access over SSH (port 22). On 
top of these default rules, docker will add its own rules. The end result is rather complex,
so I'll go through these rules step by step.

For this investigation the focus is on traffic `A` from container `A` at address `172.17.0.2` to
a webserver on container `B` at address `172.17.0.3` port `443`, both on the same docker host.
A comparison is made to external traffic `X` addressed to the same webserver on container `B` 
so hopefully we'll discover the difference that causes the issue.

## nat prerouting

Traffic from a container arrives at the host via interface `docker0` and is first processed
by the `nat` `prerouting` chain. With `iptables -t nat -L -v -n` we see the following rules 
(only the relevant rules are shown):

|   | table | chain      | target  | prot  | opt   | in       | out   | source    | destination | rule                          |
| - | ----- | -----      | -----   | ----- | ----- | -----    | ----- | -----     | -----       | -----                         |
| 1 | nat   | PREROUTING | DOCKER  | all   |       | *        | *     | 0.0.0.0/0 | 0.0.0.0/0   | ADDRTYPE match dst-type LOCAL |
| 2 | nat   | DOCKER     | RETURN  | all   |       | docker0  | *     | 0.0.0.0/0 | 0.0.0.0/0   |                               |
| 3 | nat   | DOCKER     | DNAT    | tcp   |       | !docker0 | *     | 0.0.0.0/0 | 0.0.0.0/0   | tcp dpt:443 to:172.17.0.3:443 |

The default policy for the `nat` `prerouting` chain is to accept the traffic.

**`A:`** Traffic from container `A` arrives via interface `docker0` so the line 2 applies. The `DNAT` 
rule is never reached so the destination address remains the address of the docker host itself, so this
traffic will now be routed to the `filter` `input` chain.

**`X:`** External traffic arrives via another interface, *not* `docker0`, so line 3 applies. 
The destination address is changed into the address of container `B`: 172.17.0.3:443 so this
traffic will now be routed to the `filter` `forward` chain.

## filter input

The `filter` `input` chain has not been modified by docker: these are the same rules as when docker is not running.
With `iptables -t filter -L -v -n` we see the following rules (only the relevant rules are shown):

|   | table  | chain | target | prot  | opt   | in       | out   | source    | destination | rule                             |
| - | -----  | ----- | -----  | ----- | ----- | -----    | ----- | -----     | -----       | -----                            |
| 1 | filter | INPUT | ACCEPT | all   |       | *        | *     | 0.0.0.0/0 | 0.0.0.0/0   | state RELATED,ESTABLISHED        |
| 2 | filter | INPUT | ACCEPT | icmp  |       | *        | *     | 0.0.0.0/0 | 0.0.0.0/0   |                                  |
| 3 | filter | INPUT | ACCEPT | all   |       | lo       | *     | 0.0.0.0/0 | 0.0.0.0/0   |                                  |
| 4 | filter | INPUT | ACCEPT | tcp   |       | *        | *     | 0.0.0.0/0 | 0.0.0.0/0   | state NEW tcp dpt:22             |
| 5 | filter | INPUT | REJECT | all   |       | *        | *     | 0.0.0.0/0 | 0.0.0.0/0   | reject-with icmp-host-prohibited |

The default policy for the `filter` `input` chain is to accept the traffic.

**`A:`** Traffic from container `A` is still directed at port 443 of the docker host itself, so it gets *rejected* at line 5.

## filter forward

The `filter` `forward` chain has been modified extensively by docker so it has become rather complex:
With `iptables -t filter -L -v -n` we see the following rules (only the relevant rules are shown):

|    | table  | chain                    | target                   | prot  | opt   | in       | out      | source    | destination | rule                             |
| -- | -----  | -----                    | -----                    | ----- | ----- | -----    | -----    | -----     | -----       | -----                            |
|  1 | filter | FORWARD                  | DOCKER-USER              | all   |       | *        | *        | 0.0.0.0/0 | 0.0.0.0/0   |                                  |
|  2 | filter | DOCKER-USER              | RETURN                   | all   |       | *        | *        | 0.0.0.0/0 | 0.0.0.0/0   |                                  |
|  3 | filter | FORWARD                  | DOCKER-ISOLATION-STAGE-1 | all   |       | *        | *        | 0.0.0.0/0 | 0.0.0.0/0   |                                  |
|  4 | filter | DOCKER-ISOLATION-STAGE-1 | DOCKER-ISOLATION-STAGE-2 | all   |       | docker0  | !docker0 | 0.0.0.0/0 | 0.0.0.0/0   |                                  |
|  5 | filter | DOCKER-ISOLATION-STAGE-2 | DROP                     | all   |       | *        | docker0  | 0.0.0.0/0 | 0.0.0.0/0   |                                  |
|  6 | filter | DOCKER-ISOLATION-STAGE-2 | RETURN                   | all   |       | *        | *        | 0.0.0.0/0 | 0.0.0.0/0   |                                  |
|  7 | filter | DOCKER-ISOLATION-STAGE-1 | RETURN                   | all   |       | *        | *        | 0.0.0.0/0 | 0.0.0.0/0   |                                  |
|  8 | filter | FORWARD                  | ACCEPT                   | all   |       | *        | docker0  | 0.0.0.0/0 | 0.0.0.0/0   | ctstate RELATED,ESTABLISHED      |
|  9 | filter | FORWARD                  | DOCKER                   | all   |       | *        | docker0  | 0.0.0.0/0 | 0.0.0.0/0   |                                  |
| 10 | filter | DOCKER                   | ACCEPT                   | tcp   |       | !docker0 | docker0  | 0.0.0.0/0 | 172.17.0.3  | tcp dpt:443                      |
| 11 | filter | FORWARD                  | ACCEPT                   | all   |       | docker0  | !docker0 | 0.0.0.0/0 | 0.0.0.0/0   |                                  |
| 12 | filter | FORWARD                  | ACCEPT                   | all   |       | docker0  | docker0  | 0.0.0.0/0 | 0.0.0.0/0   |                                  |
| 13 | filter | FORWARD                  | REJECT                   | all   |       | *        | *        | 0.0.0.0/0 | 0.0.0.0/0   | reject-with icmp-host-prohibited |

The default policy for the `filter` `forward` chain is to **drop** the traffic.

**`X:`** External traffic arrived via another input interface, *not* `docker0`, and the destination 
address has been changed into the address of container `B`: 172.17.0.3:443. As a result the following lines apply:

1.  Traffic goes to the `docker-user` chain.
2.  Traffic returns to the `forward` chain.
3.  Traffic goes to the `docker-isolation-stage-1` chain.
7.  Traffic returns to the `forward` chain (4 does not apply, so 5 and 6 are skipped).
8.  An existing connection is accepted by this line, new connections go to the next step.
9.  Traffic goes to the `docker` chain as the *output* interface is `docker0`.
10.  Traffic is accepted based on line 10.

**Conclusion:** external traffic is accepted while traffice from another container is *rejected* by the `filter` `input` chain.

## nat postrouting

As far as I can determine, the `nat` `postrouting` chain is not relevant here:
it is targeted at containers connecting to themselves via the address of the docker host
(these rules can be listed with `iptables -t nat -L -v -n`).

## Workarounds

The easiest solution is to allow all tcp traffic between containers in the `filter` `input` chain by adding a line allowing this traffic:

|   | table  | chain | target | prot  | opt   | in       | out     | source    | destination | rule                             |
| - | -----  | ----- | -----  | ----- | ----- | -----    | -----   | -----     | -----       | -----                            |
| 1 | filter | INPUT | ACCEPT | all   |       | *        | *       | 0.0.0.0/0 | 0.0.0.0/0   | state RELATED,ESTABLISHED        |
| 2 | filter | INPUT | ACCEPT | icmp  |       | *        | *       | 0.0.0.0/0 | 0.0.0.0/0   |                                  |
| 3 | filter | INPUT | ACCEPT | all   |       | lo       | *       | 0.0.0.0/0 | 0.0.0.0/0   |                                  |
| **4** | **filter** | **INPUT** | **ACCEPT** | **tcp** | | **docker0** | <b>*</b> | **0.0.0.0/0** | **0.0.0.0/0** |         |
| 5 | filter | INPUT | ACCEPT | tcp   |       | *        | *       | 0.0.0.0/0 | 0.0.0.0/0   | state NEW tcp dpt:22             |
| 6 | filter | INPUT | REJECT | all   |       | *        | *       | 0.0.0.0/0 | 0.0.0.0/0   | reject-with icmp-host-prohibited |

Use `iptables -t filter -L INPUT -v -n` to list just the `filter` `input` rules.

> Note that this workaround causes traffic to the container to be handled by the `docker-proxy` process handling that specific port,
instead of forwarding it via network interface `docker0` to the container.


